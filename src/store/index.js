import state from '@/store/state.js'
import actions from '@/store/actions.js'
import getters from '@/store/getters.js'
import mutations from '@/store/mutations.js'

const strict = false

export {
  state,
  actions,
  getters,
  mutations,
  strict
}
